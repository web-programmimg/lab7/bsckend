import { IsEmail, IsNotEmpty, Length, IsArray } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @Length(5, 32)
  password: string;

  @IsNotEmpty()
  @Length(5, 32)
  fullName: string;

  @IsArray()
  roles: ('admin' | 'user')[];

  @IsNotEmpty()
  gender: 'male' | 'female' | 'others';
}
