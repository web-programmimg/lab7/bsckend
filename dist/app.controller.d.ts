import { AppService } from './app.service';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    getDefault(): string;
    getHello(): string;
    getWorld(): string;
    testQuery(req: any, celsius: number, type: string): {
        celsius: number;
        type: string;
    };
    testParam(req: any, celsius: number): {
        celsius: number;
    };
    testBody(req: any, body: any, celsius: number): {
        celsius: number;
    };
}
