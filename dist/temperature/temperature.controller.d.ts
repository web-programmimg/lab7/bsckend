import { TemperatureService } from './temperature.service';
export declare class TemperatureController {
    private readonly temperatureService;
    constructor(temperatureService: TemperatureService);
    convert(celsius: string): {
        celsius: number;
        fahrenheit: number;
    };
    convertByPost(celsius: number): {
        celsius: number;
        fahrenheit: number;
    };
    convertByParam(celsius: string): {
        celsius: number;
        fahrenheit: number;
    };
}
