"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
let UsersService = class UsersService {
    constructor() {
        this.lastId = 1;
        this.users = [
            {
                id: 1,
                email: 'admin',
                password: 'Pass@1234',
                fullName: 'Admin',
                roles: ['admin'],
                gender: 'male',
            },
        ];
    }
    create(createUserDto) {
        this.lastId++;
        const newUser = { ...createUserDto, id: this.lastId };
        this.users.push(newUser);
        return newUser;
    }
    findAll() {
        return this.users;
    }
    findOne(id) {
        const index = this.users.findIndex((user) => user.id === id);
        if (index < 0) {
            throw new common_1.NotFoundException();
        }
        return this.users[index];
    }
    update(id, updateUserDto) {
        const index = this.users.findIndex((user) => user.id === id);
        if (index < 0) {
            throw new common_1.NotFoundException();
        }
        this.users[index] = { ...this.users[index], ...updateUserDto };
        return this.users[index];
    }
    remove(id) {
        const index = this.users.findIndex((user) => user.id === id);
        if (index < 0) {
            throw new common_1.NotFoundException();
        }
        const delUser = this.users[index];
        this.users.splice(index, 1);
        return delUser;
    }
};
exports.UsersService = UsersService;
exports.UsersService = UsersService = __decorate([
    (0, common_1.Injectable)()
], UsersService);
//# sourceMappingURL=users.service.js.map