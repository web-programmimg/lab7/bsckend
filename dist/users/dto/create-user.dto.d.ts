export declare class CreateUserDto {
    email: string;
    password: string;
    fullName: string;
    roles: ('admin' | 'user')[];
    gender: 'male' | 'female' | 'others';
}
