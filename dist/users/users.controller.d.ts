import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUserDto: CreateUserDto): {
        id: number;
        email: string;
        password: string;
        fullName: string;
        roles: ("admin" | "user")[];
        gender: "male" | "female" | "others";
    };
    findAll(): import("./entities/user.entity").User[];
    findOne(id: string): import("./entities/user.entity").User;
    update(id: string, updateUserDto: UpdateUserDto): import("./entities/user.entity").User;
    remove(id: string): import("./entities/user.entity").User;
}
