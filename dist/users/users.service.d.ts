import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
export declare class UsersService {
    lastId: number;
    users: User[];
    create(createUserDto: CreateUserDto): {
        id: number;
        email: string;
        password: string;
        fullName: string;
        roles: ("admin" | "user")[];
        gender: "male" | "female" | "others";
    };
    findAll(): User[];
    findOne(id: number): User;
    update(id: number, updateUserDto: UpdateUserDto): User;
    remove(id: number): User;
}
